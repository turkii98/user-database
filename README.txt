Programi su napisani za sigurno spremanje parova username i password te njihovo upravljanje
Program mgmt.py služi kao manager passworda, a login.py kao pristup određenim passwordima.
U prilogu su 2 programa, mgmt.py i login.py. Pokreću se sljedećim naredbama komandne linije.
dodavanje novog usera: "python3 mgmt.py usermgmt add <username>"
brisanje usera: "python3 mgmt.py usermgmt del <username>"
postavljanje obavezne promijene lozinke: "python3 mgmt.py usermgmt forcepass <username>"
direktna promjena lozinke: "python3 mgmt.py usermgmt passwd <username>"
login usera: "python3 login.py login <username>"