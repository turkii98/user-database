import sys
import json
from base64 import b64encode
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import scrypt
from Crypto.Hash import HMAC, SHA256
from getpass import getpass

usernames = {}
password = {}
salt = b64encode(get_random_bytes(16)).decode('utf-8')



def read():
    file = open("pwrds.txt","r")
    lines = file.readlines()
    file.close()
    print(lines)

def main():
    user = ''
    if (sys.argv[1] == "usermgmt"):
        try:
            file = open("pwrds.txt","r")
            lines = file.readlines()
            file.close()
        except:
            file = open("pwrds.txt","a+")
            lines = file.readlines()
            file.close()
        try:
            if(sys.argv[2]=="forcepass"):
                file = open("pwrds.txt", "w+")
                file.close()
                user = None
                for line in lines:
                    name = line.split()
                    file = open("pwrds.txt","a")
                    if (name[0] == sys.argv[3]):
                        user = name[0]
                        write =''
                        fPass = line.split()
                        fPassYesNo = fPass[len(fPass)-1].split(":")
                        for i in range(0, len(fPass)-1):
                            write += fPass[i] +" "
                        write += "changePass:True\n"
                        file.write(write)
                        continue
                    for el in line:
                        file.write(el)
                file.close()
                if(user == None):
                    print("there is no such user")
                else:
                    print("User",sys.argv[3], "will be requested to change password on the next login")
            if(sys.argv[2]=="del"):
                file = open("pwrds.txt", "w+")
                file.close()
                for line in lines:
                    file = open("pwrds.txt","a")
                    name = line.split()
                    if (name[0] == sys.argv[3]):
                        user = name[0]
                        continue
                    for el in line:
                        file.write(el)
                file.close
                if(user):
                    print("User",user," succesfuly removed")
                else:
                    print("There is no such user")
            if(sys.argv[2]=="add"):
                for line in lines:
                    name = line.split()
                    if(name[0]==sys.argv[3]):
                        print("Username already exists")
                        exit()
                passw = getpass()
                checkPass = getpass("Repeat Password:")
                if(passw != checkPass):
                    print("User add failed. Password missmatch.")
                    exit()
                else:
                    file
                    print("User",sys.argv[3],"succesfuly added.")
                    file = open("pwrds.txt","a")
                    salt = b64encode(get_random_bytes(16)).decode('utf-8')
                    key = scrypt(passw, salt, 16, N=2**14, r=8, p=1, num_keys=1)
                    write = sys.argv[3]," pass:",str(key)," salt:",salt, " changePass:False\n" 
                    file.writelines(write)
                    file.close()
                
            if(sys.argv[2]=="passwd"):
                    file = open("pwrds.txt", "w+")
                    file.close()
                    user = None
                    for line in lines:
                        name = line.split()
                        file = open("pwrds.txt","a")
                        if(name[0] == sys.argv[3]):
                            passw = getpass()
                            checkPass = getpass("Repeat Password:")
                            if(passw != checkPass):
                                print("Password change failed. Password missmatch.")
                                exit()
                            user = name[0]
                            write = ''
                            salt = b64encode(get_random_bytes(16)).decode('utf-8')
                            hPass =  scrypt(passw, salt, 16, N=2**14, r=8, p=1, num_keys=1)
                            write += str(name[0]) + " "
                            write += "pass:"+str(hPass) + " "
                            write += "salt:"+str(salt) +" "
                            write += str(name[3])+"\n"
                            file.write(write)
                            continue
                        for el in line:
                            file.write(el)
                    file.close()
                    if(user == None):
                        print("There is no such user.")
                    else:
                        print("Password change succseful.")
        except Exception as e:
            print(e)
            file.close()
            file = open("pwrds.txt","w+")
            print("ERROR")
            for line in lines:
                file.write(line)

    elif (sys.argv[1] =="login"):
        username = sys.argv[2]
        passw = getpass()
        print("login")

    file = open("pwrds.txt","r")
    lines = file.readlines()
    file.close()



if __name__ == "__main__":
    main()