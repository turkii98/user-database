import sys
import json
from base64 import b64encode
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import scrypt
from Crypto.Hash import HMAC, SHA256
from getpass import getpass


def main():
    if (sys.argv[1] =="login"):
        failed = True
        username = sys.argv[2]
        passw = getpass()
        file = open("pwrds.txt","r")
        lines = file.readlines()
        file.close()
        isThere = False
        for line in lines:
            user = line.split()
            if(user[0]== username):
                isThere = True
                salt = user[2].split(":")
                realPass = user[1].split(":")
                hPass =  scrypt(passw, salt[1], 16, N=2**14, r=8, p=1, num_keys=1)
                if(str(hPass) != realPass[1]):
                    print("Username or password incorrect")
                    exit()
                
                needReset = user[3].split(":")
                if(needReset[1] == 'True'):
                    newPass = getpass("New Password:")
                    repNewPass = getpass("Repeat new password:")
                    if(newPass != repNewPass):
                        print("Password change failed. Password missmatch")
                    else:
                        file = open("pwrds.txt", "w+")
                        file.close()
                        for line in lines:
                            name = line.split()
                            file = open("pwrds.txt","a")
                            if(name[0] == username):
                                salt = b64encode(get_random_bytes(16)).decode('utf-8')
                                hPass =  scrypt(newPass, salt, 16, N=2**14, r=8, p=1, num_keys=1)
                                user = name[0]
                                write = ''
                                write += str(name[0]) + " "
                                write += "pass:"+str(hPass) + " "
                                write += "salt:"+str(salt) +" "
                                write += "changePass:False\n"
                                file.write(write)
                                continue
                            for el in line:
                                file.write(el)
                        file.close()
                        print("Password change succseful.")
                        exit()
                else:
                    if(not isThere):
                        print("Username or password incorrect")
                    print("Login succesful")
        if(not isThere):
                    print("Username or password incorrect")



if __name__ == "__main__":
    main()
